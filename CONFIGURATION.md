# Configuration de l'environnement OCaml pour les TPs

On travaillera sous Linux/Fedora en salle TP.

Pour bénéficier des outils OCaml, ajouter les lignes suivantes à votre fichier `.bash_profile`;

```bash
# opam configuration
export OPAMROOT=/home/tpetu/Enseignants/emmanuel.coquery/opam
test -r /home/tpetu/Enseignants/emmanuel.coquery/opam/opam-init/init.sh && . /home/tpetu/Enseignants/emmanuel.coquery/opam/opam-init/init.sh > /dev/null 2> /dev/null || true
```

> Sur une machine personnelle, installer ocaml et opam via les paquets, par exemple sous ubuntu:
> ```
> sudo apt install ocaml opam
> ```
> sous Mac OS X avec Homebrew:
> ```
> brew install ocaml opam
> ```
>
> Dans un shell, lancer les commandes suivantes:
> ```
> opam init
> # Suivre s'il faut les instructions de configuration de l'environnement
> opam install ocaml-lsp-server
> ```

On utilisera VSCode en installant le plugin **OCaml Platform**.

Pour supprimer les lignes verticales d'indentation qui peuvent perturber al'affichage avec les `|` des `match`: aller dans le menu File -> Preferences -> Settings, entrer `editor.guide` dans la zone de recherche et décocher la case "Editor > Guides: Indentation". Vérifiez que vous êtes bien dans l'onglet "User" quand vous décochez la case. Voir le screenshot ci-dessous:

![](files/config_lignes_vscode.png)
